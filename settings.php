<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is built using the bootstrapbase template to allow for new theme's using
 * Moodle's new Bootstrap theme engine
 *
 * @package     theme_sofia
 * @copyright   2015 LMSACE Dev Team, lmsace.com
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$settings = null;

defined('MOODLE_INTERNAL') || die;
if (is_siteadmin()) {
    $ADMIN->add('themes', new admin_category('theme_sofia', 'Sofia'));

    /* General Settings */
    $temp = new admin_settingpage('theme_sofia_general', get_string('themegeneralsettings', 'theme_sofia'));

    // Logo file setting.
    $name = 'theme_sofia/logo';
    $title = get_string('logo', 'theme_sofia');
    $description = get_string('logodesc', 'theme_sofia');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Custom CSS file.
    $name = 'theme_sofia/customcss';
    $title = get_string('customcss', 'theme_sofia');
    $description = get_string('customcssdesc', 'theme_sofia');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Promoted Courses Start.
    // Promoted Courses Heading.
    $name = 'theme_sofia_promotedcoursesheading';
    $heading = get_string('promotedcoursesheading', 'theme_sofia');
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Enable / Disable Promoted Courses.
    $name = 'theme_sofia/pcourseenable';
    $title = get_string('pcourseenable', 'theme_sofia');
    $description = '';
    $default = 1;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Promoted courses Block title.
    $name = 'theme_sofia/promotedtitle';
    $title = get_string('pcourses', 'theme_sofia').' '.get_string('title', 'theme_sofia');
    $description = get_string('promotedtitledesc', 'theme_sofia');
    $default = 'lang:promotedtitledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_sofia/promotedcourses';
    $title = get_string('pcourses', 'theme_sofia');
    $description = get_string('pcoursesdesc', 'theme_sofia');
    $default = array();

    $courses[0] = '';
    $cnt = 0;
    if ($ccc = get_courses('all', 'c.sortorder ASC', 'c.id,c.shortname,c.visible,c.category')) {
        foreach ($ccc as $cc) {
            if ($cc->visible == "0" || $cc->id == "1") {
                continue;
            }
            $cnt++;
            $courses[$cc->id] = $cc->shortname;
            // Set some courses for default option.
            if ($cnt < 8) {
                $default[] = $cc->id;
            }
        }
    }
    $coursedefault = implode(",", $default);
    $setting = new admin_setting_configtextarea($name, $title, $description, $coursedefault);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    // Promoted Courses End.

    $ADMIN->add('theme_sofia', $temp);

    /* Footer Settings start */
    $temp = new admin_settingpage('theme_sofia_footer', get_string('footerheading', 'theme_sofia'));

    // Footer Block1.
    $name = 'theme_sofia_footerblock1heading';
    $heading = get_string('footerblock', 'theme_sofia').' 1 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    /* Footer Content */
    $name = 'theme_sofia/footnote';
    $title = get_string('footnote', 'theme_sofia');
    $description = get_string('footnotedesc', 'theme_sofia');
    $default = 'lang:footnotedefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Footer Block1. */

    /* Footer Block2. */
    $name = 'theme_sofia_footerblock2heading';
    $heading = get_string('footerblock', 'theme_sofia').' 2 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    $name = 'theme_sofia/footerbtitle2';
    $title = get_string('footerblock', 'theme_sofia').' '.get_string('title', 'theme_sofia').' 2 ';
    $description = get_string('footerbtitle_desc', 'theme_sofia');
    $default = 'lang:footerbtitle2default';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_sofia/footerblink2';
    $title = get_string('footerblink', 'theme_sofia').' 2';
    $description = get_string('footerblink_desc', 'theme_sofia');
    $default = get_string('footerblink2default', 'theme_sofia');
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Footer Block2 */

    /* Footer Block3 */
    $name = 'theme_sofia_footerblock3heading';
    $heading = get_string('footerblock', 'theme_sofia').' 3 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    $name = 'theme_sofia/footerbtitle3';
    $title = get_string('footerblock', 'theme_sofia').' '.get_string('title', 'theme_sofia').' 3 ';
    $description = get_string('footerbtitle_desc', 'theme_sofia');
    $default = 'lang:footerbtitle3default';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Facebook,Pinterest,Twitter,Google+ Settings */
    $name = 'theme_sofia/fburl';
    $title = get_string('fburl', 'theme_sofia');
    $description = get_string('fburldesc', 'theme_sofia');
    $default = get_string('fburl_default', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_sofia/twurl';
    $title = get_string('twurl', 'theme_sofia');
    $description = get_string('twurldesc', 'theme_sofia');
    $default = get_string('twurl_default', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_sofia/gpurl';
    $title = get_string('gpurl', 'theme_sofia');
    $description = get_string('gpurldesc', 'theme_sofia');
    $default = get_string('gpurl_default', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_sofia/pinurl';
    $title = get_string('pinurl', 'theme_sofia');
    $description = get_string('pinurldesc', 'theme_sofia');
    $default = get_string('pinurl_default', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    /* Footer Block3. */

    /* Footer Block4. */
    $name = 'theme_sofia_footerblock4heading';
    $heading = get_string('footerblock', 'theme_sofia').' 4 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Fooer Block Title 4.
    $name = 'theme_sofia/footerbtitle4';
    $title = get_string('footerblock', 'theme_sofia').' '.get_string('title', 'theme_sofia').' 4 ';
    $description = get_string('footerbtitle_desc', 'theme_sofia');
    $default = 'lang:footerbtitle4default';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Address , Phone No ,Email */
    $name = 'theme_sofia/address';
    $title = get_string('address', 'theme_sofia');
    $description = '';
    $default = get_string('defaultaddress', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_sofia/phoneno';
    $title = get_string('phoneno', 'theme_sofia');
    $description = '';
    $default = get_string('defaultphoneno', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_sofia/emailid';
    $title = get_string('emailid', 'theme_sofia');
    $description = '';
    $default = get_string('defaultemailid', 'theme_sofia');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    /* Footer Block4 */

    $ADMIN->add('theme_sofia', $temp);
    /*  Footer Settings end */

    /* Marketing Spots */
    $temp = new admin_settingpage('theme_sofia_marketingspots', get_string('marketingspotsheading', 'theme_sofia'));

    /* Marketing Spot 1*/
    $name = 'theme_sofia_mspot1heading';
    $heading = get_string('marketingspot', 'theme_sofia').' 1 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    // Marketing Spot 1 Icon.
    $name = 'theme_sofia/mspot1icon';
    $title = get_string('marketingspot', 'theme_sofia').' 1 - '.get_string('icon', 'theme_sofia');
    $description = get_string('faicondesc', 'theme_sofia');
    $default = 'globe';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 1 Title.
    $name = 'theme_sofia/mspot1title';
    $title = get_string('marketingspot', 'theme_sofia').' 1 - '.get_string('title', 'theme_sofia');
    $description = get_string('mspottitledesc', 'theme_sofia');
    $default = 'lang:mspot1titledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 1 Description.
    $name = 'theme_sofia/mspot1desc';
    $title = get_string('marketingspot', 'theme_sofia').' 1 - '.get_string('description');
    $description = get_string('mspotdescdesc', 'theme_sofia');
    $default = 'lang:mspot1descdefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Marketing Spot 1*/

    /* Marketing Spot 2*/
    $name = 'theme_sofia_mspot2heading';
    $heading = get_string('marketingspot', 'theme_sofia').' 2 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    // Marketing Spot 2 Icon.
    $name = 'theme_sofia/mspot2icon';
    $title = get_string('marketingspot', 'theme_sofia').' 2 - '.get_string('icon', 'theme_sofia');
    $description = get_string('faicondesc', 'theme_sofia');
    $default = 'graduation-cap';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 2 Title.
    $name = 'theme_sofia/mspot2title';
    $title = get_string('marketingspot', 'theme_sofia').' 2 - '.get_string('title', 'theme_sofia');
    $description = get_string('mspottitledesc', 'theme_sofia');
    $default = 'lang:mspot2titledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    // Marketing Spot 2 Description.
    $name = 'theme_sofia/mspot2desc';
    $title = get_string('marketingspot', 'theme_sofia').' 2 - '.get_string('description');
    $description = get_string('mspotdescdesc', 'theme_sofia');
    $default = 'lang:mspot2descdefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Marketing Spot 2*/

    /* Marketing Spot 3*/
    $name = 'theme_sofia_mspot3heading';
    $heading = get_string('marketingspot', 'theme_sofia').' 3 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    // Marketing Spot 3 Icon.
    $name = 'theme_sofia/mspot3icon';
    $title = get_string('marketingspot', 'theme_sofia').' 3 - '.get_string('icon', 'theme_sofia');
    $description = get_string('faicondesc', 'theme_sofia');
    $default = 'bullhorn';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 3 Title.
    $name = 'theme_sofia/mspot3title';
    $title = get_string('marketingspot', 'theme_sofia').' 3 - '.get_string('title', 'theme_sofia');
    $description = get_string('mspottitledesc', 'theme_sofia');
    $default = 'lang:mspot3titledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 3 Description.
    $name = 'theme_sofia/mspot3desc';
    $title = get_string('marketingspot', 'theme_sofia').' 3 - '.get_string('description');
    $description = get_string('mspotdescdesc', 'theme_sofia');
    $default = 'lang:mspot3descdefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Marketing Spot 3*/

    /* Marketing Spot 4*/
    $name = 'theme_sofia_mspot4heading';
    $heading = get_string('marketingspot', 'theme_sofia').' 4 ';
    $information = '';
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    // Marketing Spot 4 Icon.
    $name = 'theme_sofia/mspot4icon';
    $title = get_string('marketingspot', 'theme_sofia').' 4 - '.get_string('icon', 'theme_sofia');
    $description = get_string('faicondesc', 'theme_sofia');
    $default = 'mobile';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 4 Title.
    $name = 'theme_sofia/mspot4title';
    $title = get_string('marketingspot', 'theme_sofia').' 4 - '.get_string('title', 'theme_sofia');
    $description = get_string('mspottitledesc', 'theme_sofia');
    $default = 'lang:mspot4titledefault';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Marketing Spot 4 Description.
    $name = 'theme_sofia/mspot4desc';
    $title = get_string('marketingspot', 'theme_sofia').' 4 - '.get_string('description');
    $description = get_string('mspotdescdesc', 'theme_sofia');
    $default = 'lang:mspot4descdefault';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    /* Marketing Spot 4*/

    $ADMIN->add('theme_sofia', $temp);
    /* Marketing Spots */

    // Slideshow Settings Start.
    $temp = new admin_settingpage('theme_sofia_slideshow', get_string('slideshowheading', 'theme_sofia'));
    $temp->add(new admin_setting_heading('theme_sofia_slideshow', get_string('slideshowheadingsub', 'theme_sofia'),
    format_text(get_string('slideshowdesc', 'theme_sofia'), FORMAT_MARKDOWN)));

    // Display Slideshow.
    $name = 'theme_sofia/toggleslideshow';
    $title = get_string('toggleslideshow', 'theme_sofia');
    $description = get_string('toggleslideshowdesc', 'theme_sofia');
    $yes = get_string('yes');
    $no = get_string('no');
    $default = 1;
    $choices = array(1 => $yes , 0 => $no);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Number of slides.
    $name = 'theme_sofia/numberofslides';
    $title = get_string('numberofslides', 'theme_sofia');
    $description = get_string('numberofslides_desc', 'theme_sofia');
    $default = 3;
    $choices = array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
    );
    $temp->add(new admin_setting_configselect($name, $title, $description, $default, $choices));

    $numberofslides = get_config('theme_sofia', 'numberofslides');
    for ($i = 1; $i <= $numberofslides; $i++) {

        // This is the descriptor for Slide One.
        $name = 'theme_sofia/slide' . $i . 'info';
        $heading = get_string('slideno', 'theme_sofia', array('slide' => $i));
        $information = get_string('slidenodesc', 'theme_sofia', array('slide' => $i));
        $setting = new admin_setting_heading($name, $heading, $information);
        $temp->add($setting);

        // Slide Image.
        $name = 'theme_sofia/slide' . $i . 'image';
        $title = get_string('slideimage', 'theme_sofia');
        $description = get_string('slideimagedesc', 'theme_sofia');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide' . $i . 'image');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide Caption.
        $name = 'theme_sofia/slide' . $i . 'caption';
        $title = get_string('slidecaption', 'theme_sofia');
        $description = get_string('slidecaptiondesc', 'theme_sofia');
        $default = 'lang:slidecaptiondefault';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide Description.
        $name = 'theme_sofia/slide' . $i . 'desc';
        $title = get_string('slidedesc', 'theme_sofia');
        $description = get_string('slidedescdesc', 'theme_sofia');
        $default = 'lang:knowmore';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

        // Slide Link text.
        $name = 'theme_sofia/slide' . $i . 'urltext';
        $title = get_string('slideurltext', 'theme_sofia');
        $description = get_string('slideurltextdesc', 'theme_sofia');
        $default = 'lang:knowmore';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_TEXT);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

		// Slide destination link

		$name = 'theme_sofia/slide'.$i.'url';
        $title = get_string('slideurl', 'theme_sofia');
        $description = get_string('slideurldesc', 'theme_sofia');
        $default = 'http://www.example.com/';
        $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);

    }

    $ADMIN->add('theme_sofia', $temp);
    /* Slideshow Settings End*/

}