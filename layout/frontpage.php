<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_sofia
 * @copyright 2015 LMSACE Dev Team,lmsace.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
global $PAGE;
$html = theme_sofia_get_html_for_settings($OUTPUT, $PAGE);

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

$courserenderer = $PAGE->get_renderer('core', 'course');

echo $OUTPUT->doctype();

$pcourseenable = theme_sofia_get_setting('pcourseenable');
$toggleslideshow = theme_sofia_get_setting('toggleslideshow');
if(!empty($pcourseenable) || !empty($toggleslideshow))
{
	$PAGE->requires->css(theme_sofia_theme_css('swiper.min')); 
}
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php  require_once(dirname(__FILE__) . '/includes/header.php');  ?>
<!--E.O.Custom theme header-->


<?php
if ($toggleslideshow == 1) 
{
    require_once(dirname(__FILE__) . '/includes/slideshow.php');
}
?>

<?php require_once(dirname(__FILE__) . '/includes/marketingspots.php'); ?>

<?php echo $courserenderer->promoted_courses(); ?>

<div id="page">
    <header id="page-header" class="clearfix">
	    
	    <div id="page-navbar" class="clearfix">
            <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
            <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
        </div>
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
        </div>
    </header>
    <div id="page-content">
    
        <div id="<?php echo $regionbsid ?>">
            <?php
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </div>
    
    </div>
</div>


<?php

if(!empty($pcourseenable) || !empty($toggleslideshow))
{
	?>
	<script src="<?php echo theme_sofia_theme_url(); ?>/javascript/swiper.min.js"></script>
	<?php
}

if ($toggleslideshow == 1) {
	?>
	<script type="text/javascript">
	    var swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        effect: 'slide',
	        loop : true,
	        speed : 1500,
	        onSlideChangeStart:function(sw)
	        {
	        	let prev = sw.slides[sw.previousIndex];
	        	let prevImage = $(prev).css('background-image');
	        	$('#preview-prev').css('background-image', prevImage);
	        	
	        	let nextIndex = sw.slides.length > (sw.realIndex+1)? (sw.realIndex + 2) : 0;
	        	let next = sw.slides[nextIndex];
	        	let nextImage = $(next).css('background-image');
	        	$('#preview-next').css('background-image', nextImage);
	        }
	    });
	</script>
	<?php
}

if (!empty($pcourseenable)) 
{
	?>
	<script type="text/javascript">
	    var swiper = new Swiper('.promoted-courses', {
	        slidesPerView: 6,
	        paginationClickable: true,
	        spaceBetween: 10
	    });
	</script>
	<?php
}

require_once(dirname(__FILE__) . '/includes/footer.php');  ?>   
<!--E.O.Custom theme footer-->

</body>
</html>
