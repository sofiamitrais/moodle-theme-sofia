<!-- Swiper --> 
<?php

$numberofslides = theme_sofia_get_setting('numberofslides');
if ($numberofslides) 
{
    ?>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <?php 
            $prevImage = null;
            $nextImage = null;
            for($s1 = 1; $s1 <= $numberofslides; $s1++)
            {
                $clstxt2 = ($s1 == "1") ? ' active' : '';
                
                $slidecaption = theme_sofia_get_setting('slide' . $s1 . 'caption', true);
                $slidecaption = theme_sofia_lang($slidecaption);
                
                $slideurl = theme_sofia_get_setting('slide' . $s1 . 'url');
                $slideurltext = theme_sofia_get_setting('slide' . $s1 . 'urltext');
                $slideurltext = theme_sofia_lang($slideurltext);
                
                $slidedesc = theme_sofia_get_setting('slide' . $s1 . 'desc');
                $slidedesc = theme_sofia_lang($slidedesc);

                $slideimg = theme_sofia_render_slideimg($s1, 'slide' . $s1 . 'image');
                if($s1 == $numberofslides)
                {
                    $prevImage = $slideimg;
                }
                if($s1 == 2)
                {
                    $nextImage = $slideimg;
                }
                ?>
                <div class="swiper-slide" style="background-image:url(<?php echo $slideimg; ?>)">
                    <div class="swipe-box slide-up">
                        <h2><?php echo $slidecaption; ?></h2>
                        <p><?php echo $slidedesc; ?></p>
                        <a href="<?php echo $slideurl; ?>" class="button button-md button-secondary"><?php echo $slideurltext; ?></a>
                    </div>  
                </div>
                <?php
            }
            ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination swiper-pagination-white"></div>
        <!-- Add Arrows -->
        
        <div class="swiper-row">
        <div class="swiper-button-prev swiper-btn">
            <span class="fa fa-angle-left"></span>
        </div>
        <?php
            if($prevImage !== null)
            {
                ?>
                <div class="preview prev" id="preview-prev" style="background-image: url(<?php echo $prevImage; ?>)">
                </div>
                <?php
            }
        ?>
        </div>

        <div class="swiper-row">
        <div class="swiper-button-next swiper-btn">
            <span class="fa fa-angle-right"></span>
        </div>
        <?php
            if($nextImage !== null)
            {
                ?>
                <div class="preview next" id="preview-next" style="background-image: url(<?php echo $nextImage; ?>)">
                </div>
                <?php
            }
        ?>
        </div>

    </div>
    <?php
}
?>